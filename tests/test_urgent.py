#!/usr/bin/env python

import gtk

def createwnd(i):
    w = gtk.Window(gtk.WINDOW_TOPLEVEL)
    w.set_title("Urgency Test %d" % i)
    w.connect("delete-event", lambda x, y: gtk.main_quit())

    hb = gtk.HBox()
    button = gtk.ToggleButton("Urgency")
    button.connect("clicked", lambda x: w.set_urgency_hint(not w.get_urgency_hint()))
    hb.pack_end(button)

    w.set_border_width(10)
    w.add(hb)
    w.show_all()

for x in range(3):
    createwnd(x)
 
gtk.main()

#end
