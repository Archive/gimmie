#!/usr/bin/python
import os
import sys
import urllib2
from gettext import gettext as _

import gobject
import gtk
import gnomekeyring

from gimmie_base import Item, ItemSource
from gimmie_threads import threadpool, Pool
from gimmie_util import gconf_bridge

import gdata.service
import gdata.docs.service


goffice_threads = Pool(4)


class GOfficeAccountWindow(gobject.GObject):
    __gsignals__ = {
        "changed" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
    }
    
    def __init__(self):
        gobject.GObject.__init__(self)

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self._construct_ui()
        
        self.window.connect("delete_event", lambda s, w: self.window.destroy())
        
        self.accounts = []
        
    def _construct_ui(self):
        self.window.set_title(_("Login to Google Office"))
        self.window.set_border_width(12)

        vbox = gtk.VBox(spacing=18)
        self.window.add(vbox)

        frame = gtk.Frame()
        frame.set_shadow_type(gtk.SHADOW_NONE)
        vbox.pack_start(frame)
        
        label = gtk.Label()
        label.set_markup("<b>" + _("Account Settings") + "</b>")
        frame.set_label_widget(label)
        frame.set_label_align(xalign=0.0, yalign=0.50)
        
        alignment = gtk.Alignment(xalign=0.50, yalign=0.50,
                                  xscale=1.0, yscale=1.0)
        alignment.set_padding(padding_top=5, padding_bottom=0,
                              padding_left=12, padding_right=0)
        frame.add(alignment)
        
        vbox2 = gtk.VBox(spacing=6)
        alignment.add(vbox2)
        
        label = gtk.Label()
        label.set_text(_("Sign in to Google Office with your Google Account"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        vbox2.pack_start(label, expand=False)

        # Create table for Username and Pasword entries
        table = gtk.Table(rows=2, columns=2)
        table.set_row_spacings(6)
        table.set_col_spacings(12)
        vbox2.pack_start(table)
        
        label = gtk.Label(_("Username:"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        table.attach(label, left_attach=0, right_attach=1, top_attach=0,
                     bottom_attach=1, xoptions=gtk.FILL, yoptions=0)
        
        label = gtk.Label(_("Password:"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        table.attach(label, left_attach=0, right_attach=1, top_attach=1,
                     bottom_attach=2, xoptions=gtk.FILL, yoptions=0)

        self.username_entry = gtk.Entry()
        table.attach(self.username_entry, left_attach=1, right_attach=2,
                     top_attach=0, bottom_attach=1, yoptions=0)
        
        self.password_entry = gtk.Entry()
        self.password_entry.set_visibility(False)
        self.password_entry.connect("activate", self._button_ok_clicked)
        table.attach(self.password_entry, left_attach=1, right_attach=2,
                     top_attach=1, bottom_attach=2, yoptions=0)
        
        btn_box = gtk.HButtonBox()
        btn_box.set_spacing(6)
        btn_box.set_layout(gtk.BUTTONBOX_END)
        vbox.pack_start(btn_box, expand=False)
        
        btn = gtk.Button(stock=gtk.STOCK_CANCEL)
        btn.connect("clicked", lambda w: self.window.hide())
        btn_box.pack_start(btn)
        
        btn = gtk.Button(stock=gtk.STOCK_OK)
        btn.connect("clicked", self._button_ok_clicked)
        btn_box.pack_start(btn)
        
    def _init_account_settings(self):
        self.keyring = gnomekeyring.get_default_keyring_sync()        

        token = gconf_bridge.get("gmail_keyring_token")
        if not token > 0:
            return

        try:
            passwords = gnomekeyring.item_get_info_sync(self.keyring, token).get_secret()
        except gnomekeyring.Error:
            return

        try:
            self.accounts = passwords.split(';')

            # Only display first account values
            username, password = self.accounts[0].split(':')
            self.username_entry.set_text(username)
            self.password_entry.set_text(password)
        except ValueError:
            return
        
    def _button_ok_clicked(self, btn):    
        username = self.username_entry.get_text()
        password = self.password_entry.get_text()

        # Replace first account with new the new values.
        secret = [ ":".join((username, password)) ] + self.accounts[1:]

        try:
            token = gnomekeyring.item_create_sync(self.keyring,
                                                  gnomekeyring.ITEM_GENERIC_SECRET,
                                                  "Gimmie Google Office account information",
                                                  dict(), ";".join(secret), True)
            gconf_bridge.set("gmail_keyring_token", int(token))
            self.emit("changed")
        except gnomekeyring.Error:
            print " !!! Unable to securely store Google Office account details"

        self.window.hide()
        
    def show(self):
        self._init_account_settings()

        self.username_entry.grab_focus()
        self.window.show_all()


class GOfficeAccountSettings(Item):
    def __init__(self):
        Item.__init__(self,
                      name=_("Login to Google Office"),
                      icon="stock_person")

        self.account_win = GOfficeAccountWindow()
        self.account_win.connect("changed", lambda x: self.emit("reload"))
        
    def do_open(self):
        self.account_win.show()


class GoogleOfficeFile(Item):
    
    def __init__(self, file):
        Item.__init__(self,
                      uri=file.link[0].href,
                      name=file.title.text,
                      comment=file.author[0].name.text +" "+file.updated.text,
                      icon="stock_new-spreadsheet",
                      mimetype="googleoffice/document")


class GOfficeSource(ItemSource):
    '''
    A list of Google Office files contacts
    '''
    
    def __init__(self):
        ItemSource.__init__(self,
                            name=_("Google Docs"),
                            uri="source://Library/Google Office",
                            icon="stock_new-spreadsheet",
                            filter_by_date=False)

        goffice_reader.connect("reload", lambda x: self.emit("reload"))
        self.load_icon()

    @threadpool(goffice_threads)
    def load_icon(self):
        try:
            picurl = urllib2.urlopen("http://docs.google.com/favicon.ico")
            pic = gtk.gdk.PixbufLoader()
            pic.write(picurl.read())
            pic.close()

            gtk.gdk.threads_enter()
            self.icon = pic.get_pixbuf()
            self.emit("reload")
            gtk.gdk.threads_leave()
        except IOError:
            pass

    def get_items_uncached(self):
        if not goffice_reader.logged_in:
            yield goffice_reader.get_account_settings()
        else:
            for item in goffice_reader.get_files():
                yield item


class GOfficeAccountReader(gobject.GObject):
    __gsignals__ = {
        "reload" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
    }

    def __init__(self):
        gobject.GObject.__init__(self)

        self.account_list = []

        self.account_settings = GOfficeAccountSettings()

        # Detect when account settings have changed.
        # We need to use two different ways to detect when account settings have
        # changed depending if the gconf key gmail_keyring_token has been set or
        # not. If we only connect to the "reload" signal in GmailAccountSettings
        # there is a race condition reading the gconf key in _login() when the
        # gconf is set for the first time.
        if gconf_bridge.get("gmail_keyring_token") > 0:
            self.account_settings.connect("reload", lambda x: self.emit("reload"))
        else:
            gconf_bridge.connect("changed::gmail_keyring_token",
                                 lambda gb: self._gconf_gmail_keyring_token_set())

        self.contacts_path = os.path.expanduser("~/.gimmie-contacts")

        self.logged_in = False

    def _gconf_gmail_keyring_token_set(self):
        if gconf_bridge.get("gmail_keyring_token") > 0:
            self.account_settings.connect("reload", lambda x: self.emit("reload"))
            self.emit("reload")

    def do_reload(self):
        self._login()

    def _login(self):
        self.account_list = []

        keyring_token = gconf_bridge.get("gmail_keyring_token")

        # Show the Gmail Account Setting Window if no account has been set up.
        if keyring_token <= 0:
            self.account_settings.do_open()
            return

        try:
            keyring = gnomekeyring.get_default_keyring_sync()
            passwords = gnomekeyring.item_get_info_sync(keyring, keyring_token).get_secret()
        except gnomekeyring.Error:
            print " !!! Access to GMail account details denied"
            self.logged_in = False
            return

        accounts = passwords.split(';')

        cancel = False
        for acct in accounts:
            if cancel:
                break
            username, password = acct.split(':')

            successful = False
            while not successful:
                try:
                    ga = gdata.docs.service.DocsService()
                    # Authenticate using your Google Docs email address and password.
                    ga.ClientLogin(username, password)
                    successful = True
                    self.account_list.append(ga)
                except gdata.service.Error, err:
                    msg_text = _('Login failed for %s:\n%s') % (username, err)
                    dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, message_format=msg_text)

                    if len(accounts) > 1:
                        dialog.add_button( _('_Ignore'), gtk.RESPONSE_REJECT)
                    dialog.add_button( gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
                    dialog.add_button(_('_Retry'), gtk.RESPONSE_ACCEPT)
                    dialog.set_default_response(gtk.RESPONSE_ACCEPT)
                    response = dialog.run()
                    dialog.destroy()

                    # Skip this account and go to next account
                    if response == gtk.RESPONSE_REJECT:
                        break

                    if response == gtk.RESPONSE_CANCEL or \
                       response == gtk.RESPONSE_DELETE_EVENT:
                        cancel = True
                        break

                    # Try logging into the account again
                    continue

        self.logged_in = True

    def get_account_settings(self):
        return self.account_settings

    def get_files(self):
        if self.logged_in == False:
            return []

        files = []

        for acct in self.account_list:
           documents_feed = acct.GetDocumentListFeed() 
           for document_entry in documents_feed.entry:
               file = GoogleOfficeFile(document_entry)
               files.append(file)

        return files


#
# Globals
#

goffice_reader = GOfficeAccountReader()
