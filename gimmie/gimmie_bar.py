#!/usr/bin/env python

from gettext import gettext as _

import gtk

from gimmie_base import IOrientationAware, gimmie_get_topic_for_uri
from gimmie_gui import EdgeWindow, AnchoredWindow, TopicRunningList
from gimmie_util import FocusRaiser, NoWindowButton, KillFocusPadding
from gimmie_topicwin import TopicView, ZoomMenuToolItem


class HideOnDeleteWindow(gtk.Window):
    __gsignals__ = {
        'hide' : 'override',
        'delete-event' : 'override',
        'key-press-event' : 'override',
        }

    def __init__(self):
        gtk.Window.__init__(self, gtk.WINDOW_TOPLEVEL)

        # Act the same as hidden when iconified by hiding from the task list.
        self.connect("map-event", lambda w, ev: self.set_skip_taskbar_hint(False))
        self.connect("unmap-event", lambda w, ev: self.set_skip_taskbar_hint(True))

    def do_hide(self):
        self.chain()

        # Workaround Gtk bug, where adding or changing Widgets
        # while the Window is hidden causes it to be reshown at
        # 0,0...
        self_x, self_y = self.get_position()
        self.move(self_x, self_y)
        return True

    def do_delete_event(self, ev):
        self.hide()
        return True

    def do_key_press_event(self, ev):
        if not gtk.Window.do_key_press_event(self, ev) \
               and ev.keyval == gtk.gdk.keyval_from_name("Escape"):
            self.iconify()
            return True


class TopicWindow(HideOnDeleteWindow, TopicView):
    '''
    The toplevel window representing a Topic\'s ItemSources, allowing one to be
    active at a time and displaying it\'s item contents using an ItemIconView.
    '''
    def __init__(self, topic):
        HideOnDeleteWindow.__init__(self)
        TopicView.__init__(self, topic)

        self.set_title(topic.get_name())
        self.set_position(gtk.WIN_POS_CENTER)
        self.set_default_size(730, -1)

        self.add_accel_group(self.accel_group)

        ### Uncomment to use the topic's color as a border
        #self.modify_bg(gtk.STATE_NORMAL, topic.get_hint_color())

        # Vbox containing the toolbar and content
        vbox = gtk.VBox(False, 0)
        vbox.show()
        self.add(vbox)

        # Toolbar
        vbox.pack_start(self.toolbar, False, False, 0)

        # Contains the visual frame, giving it some space
        content = gtk.HBox(False, 0)
        content.set_border_width(12)
        content.show()
        vbox.add(content)

        # Hbox containing the sidebar buttons and the iconview
        body = gtk.HBox(False, 12)
        body.show()
        content.pack_start(body, True, True, 0)

        # Load up the sidebar
        body.pack_start(self.sidebar, False, False, 0)

        # Add frame containing the icon view
        body.pack_start(self.content_frame, True, True, 0)

        # Iconview for the current sidebar selection
        self.view.connect("item-activated", lambda v, p: self.iconify()) # Iconify on item open

        # Zoom drop down list
        self.zoom_menu = ZoomMenuToolItem()
        self.zoom_menu.set_tooltip(self.tooltips, _("Set the zoom level"))
        self.zoom_menu.set_is_important(True)
        self.zoom_menu.connect("zoom-changed", lambda w, num_days: self.zoom_changed(num_days))
        self.zoom_menu.connect("open-timeline", lambda w: self._open_timeline())

        ### Uncomment to make parts of the window draggable (only toolbar currently)
        #self.connect_after("button-press-event",
        #                   lambda w, ev: self.begin_move_drag(ev.button,
        #                                                      int(ev.x_root),
        #                                                      int(ev.y_root),
        #                                                      ev.time))

        # Setup the toolbar
        self._add_toolbar_items()

        # Select an initial sidebar button
        self.find_first_button()

    def get_zoom_level(self):
        return self.zoom_menu.get_zoom_level()

    def get_zoom_level_list(self):
        return self.zoom_menu.get_zoom_level_list()

    def set_zoom_level(self, zoom):
        self.zoom_menu.set_zoom_level(zoom)

    def show_hide_zoomer(self, show):
        self.zoom_menu.set_sensitive(show)

    def _open_timeline(self):
        topic = gimmie_get_topic_for_uri("topic://Computer")
        topicwin = topic.get_topic_window()
        topicwin.set_source_by_uri("source://Timeline")
        topicwin.present()

    def _add_toolbar_items(self):
        for i in self.topic.get_toolbar_items(self.tooltips):
            if not i:
                i = gtk.SeparatorToolItem()
            i.show_all()
            self.toolbar.insert(i, -1)

        # Right-align the zoom and search tool items
        sep = gtk.SeparatorToolItem()
        sep.set_draw(False)
        sep.set_expand(True)
        sep.show()
        self.toolbar.insert(sep, -1)

        self.toolbar.insert(self.zoom_menu, -1)

        self.add_search_toolitem()


class TopicButton(gtk.Button):
    __gsignals__ = {
        'size-allocate' : 'override',
        'button-press-event' : 'override'
        }

    def __init__(self, topic, edge_gravity):
        gtk.Button.__init__(self)
        self.modify_bg(gtk.STATE_NORMAL, topic.get_hint_color())
        self.set_property("can-default", False)
        self.set_property("can-focus", False)
        self.set_border_width(0)
        
        KillFocusPadding(self, "topic-button")
        
        label = topic.get_button_content(edge_gravity)
        label.show()
        self.add(label)

        ### FIXME: Figure out why button adds 2px of padding
        #self.set_size_request(-1, 24)
        
        self.topic = topic
        self.topic_win = TopicWindow(self.topic)
        self.topic.set_topic_window(self.topic_win)

    def do_set_wm_icon_geometry(self):
        '''
        Set the _NET_WM_ICON_GEOMETRY window manager hint, so that the topic
        window will minimize onto this button\'s allocatied area.  See
        http://standards.freedesktop.org/wm-spec/latest for details.
        '''
        if self.window and self.topic_win and self.topic_win.window:
            win_x, win_y = self.window.get_origin()

            # values are left, right, width, height
            propvals = [win_x + self.allocation.x,
                        win_y + self.allocation.y,
                        self.allocation.width,
                        self.allocation.height]

            # tell window manager where to animate minimizing this app
            self.topic_win.window.property_change("_NET_WM_ICON_GEOMETRY",
                                                  "CARDINAL",
                                                  32,
                                                  gtk.gdk.PROP_MODE_REPLACE,
                                                  propvals)

    def do_clicked(self):
        self.do_set_wm_icon_geometry()
        self.topic_win.deiconify()
        self.topic_win.present()

    def do_size_allocate(self, alloc):
        ret = self.chain(alloc)
        self.do_set_wm_icon_geometry()
        return ret

    def do_button_press_event(self, ev):
        if ev.button == 3:
            menu = gtk.Menu()

            for mi in self.topic.get_context_menu_items():
                if not mi:
                    mi = gtk.SeparatorMenuItem()
                    mi.show()
                menu.append(mi)

            menu.connect('selection-done', lambda x: menu.destroy())

            menu.attach_to_widget(self, None)
            menu.popup(None, None, None, ev.button, ev.time)

            return True
        else:
            return self.chain(ev)


class GimmieBar:
    '''
    Abstract baseclass for different layout implementations.  Subclasses must
    implement the layout() call.
    '''
    def __init__(self, topic_list, edge_gravity, autohide_anchors, swapbar):
        self.topic_list = topic_list
        self.autohide_anchors = autohide_anchors

        self.edge_window = EdgeWindow(edge_gravity)
        self.layout(edge_gravity, self.edge_window, swapbar)

    def make_spacer(self, edge_gravity, size = 24):
        sep = gtk.EventBox()
        if edge_gravity in (gtk.gdk.GRAVITY_EAST, gtk.gdk.GRAVITY_WEST):
            sep.set_size_request(-1, size)
        else:
            sep.set_size_request(size, -1)
        sep.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
        sep.show()
        return sep

    def make_topic_button(self, edge_gravity, topic):
        btn = TopicButton(topic, edge_gravity)
        btn.show()
        return btn

    def make_running_list(self, edge_gravity, topic):
        running_source = topic.get_running_source()
        if isinstance(running_source, IOrientationAware):
            if edge_gravity in (gtk.gdk.GRAVITY_WEST, gtk.gdk.GRAVITY_EAST):
                orientation = gtk.ORIENTATION_VERTICAL
            else:
                orientation = gtk.ORIENTATION_HORIZONTAL
            running_source.set_orientation(orientation)
        running = TopicRunningList(running_source, edge_gravity)
        running.show()
        return running

    def destroy(self):
        self.edge_window.destroy()

    def layout(self, edge_gravity, edge_window, swapbar):
        raise NotImplementedError


class GimmieBarDock(GimmieBar):
    '''
    A GimmieBar with topic\'s running icons or topic buttons arranged on the
    screen edge, with the topic buttons or running icons hovering next to,
    below, or above.
    '''
    def layout(self, edge_gravity, edge_window, swapbar):
        if edge_gravity in [gtk.gdk.GRAVITY_WEST, gtk.gdk.GRAVITY_EAST]:
            # Keep the running items lists all the same width
            anchor_size_group = gtk.SizeGroup(gtk.SIZE_GROUP_HORIZONTAL)
        else:
            # Keep the running items lists all the same height
            anchor_size_group = gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL)

        first = True

        for topic in self.topic_list:
            if topic == None:
                edge_window.content.pack_start(self.make_spacer(edge_gravity), False, False, 0)
            else:
                if not first and not swapbar:
                    edge_window.content.pack_start(self.make_spacer(edge_gravity, 1),
                                                   False, False, 0)
                first = False

                # Give some space between topics
                align = gtk.Alignment(xscale=1.0, yscale=1.0)
                if edge_gravity in (gtk.gdk.GRAVITY_WEST, gtk.gdk.GRAVITY_EAST):
                    align.set_property("top-padding", 4)
                    align.set_property("bottom-padding", 4)
                else:
                    align.set_property("top-padding", 1)
                    align.set_property("left-padding", 4)
                    align.set_property("right-padding", 4)

                running_list = self.make_running_list(edge_gravity, topic)
                align.add(running_list)
                align.show()

                if swapbar:
                    running_btn = NoWindowButton()
                else:
                    running_btn = gtk.EventBox()
                running_btn.modify_bg(gtk.STATE_NORMAL, topic.get_hint_color())
                running_btn.add(align)
                running_btn.show()

                drag_flags = gtk.DEST_DEFAULT_DROP | gtk.DEST_DEFAULT_MOTION
                
                if topic.accept_drops():
                    drag_flags |= gtk.DEST_DEFAULT_HIGHLIGHT

                running_btn.drag_dest_set(drag_flags,
                                          [("text/uri-list", 0, 100)],
                                          gtk.gdk.ACTION_COPY)
                           
                running_btn.connect("drag-data-received",
                                    self._drag_data_received,
                                    topic.get_running_source())
                
                if not swapbar:
                    ev_box = gtk.EventBox()
                    ev_box.add(running_btn)
                    ev_box.show()

                # Topic button, opens the topic window
                topic_btn = self.make_topic_button(edge_gravity, topic)
                if swapbar:
                    edge_window.content.pack_start(topic_btn, True, True, 0)
                else:
                    edge_window.content.pack_start(ev_box, True, True, 0)

                if swapbar:
                    anchor_win = AnchoredWindow(edge_gravity, topic_btn)
                    anchor_win.content.pack_start(running_btn, True, True, 0)
                else:
                    anchor_win = self.make_anchor_win(topic_btn, running_btn, edge_gravity)
                # Keep the running items lists all the same height or width
                anchor_size_group.add_widget(anchor_win)

                sizing_type = gtk.SIZE_GROUP_HORIZONTAL # Manage widths
                if edge_gravity in (gtk.gdk.GRAVITY_WEST, gtk.gdk.GRAVITY_EAST):
                    sizing_type = gtk.SIZE_GROUP_VERTICAL # Manage heights

                # Using a sizing group to ensure the edge buttons will be as
                # large as possible.
                self.size_group = gtk.SizeGroup(sizing_type)
                self.size_group.add_widget(running_btn)
                self.size_group.add_widget(topic_btn)

                # Avoid showing anchored window too early
                if not self.autohide_anchors:
                    edge_window.connect_after("show", lambda x, aw: aw.show(), anchor_win)
                edge_window.connect("destroy", lambda x, aw: aw.destroy(), anchor_win)

                if swapbar:
                    raiser = FocusRaiser(topic_btn, anchor_win)
                else:
                    raiser = FocusRaiser(running_btn, anchor_win)
                raiser.set_gravity(edge_gravity)
                raiser.set_hide_on_lower(self.autohide_anchors)

	if not swapbar:
            self.setup_edge_win(edge_window, edge_gravity)

        # Shows everything (including anchor windows)
        edge_window.show()

    def setup_edge_win(self, edge_window, edge_gravity):
        align = edge_window.get_content_alignment()
        # 1px on either non-edge side
        if edge_gravity in (gtk.gdk.GRAVITY_EAST, gtk.gdk.GRAVITY_WEST):
            if self.autohide_anchors:
                if edge_gravity == gtk.gdk.GRAVITY_EAST:
                    align.set_property("left-padding", 1)
                elif edge_gravity == gtk.gdk.GRAVITY_WEST:
                    align.set_property("right-padding", 1)
            align.set_property("top-padding", 1)
            align.set_property("bottom-padding", 1)
        else:
            if self.autohide_anchors:
                if edge_gravity == gtk.gdk.GRAVITY_NORTH:
                    align.set_property("bottom-padding", 1)
                elif edge_gravity == gtk.gdk.GRAVITY_SOUTH:
                    align.set_property("top-padding", 1)
            align.set_property("left-padding", 1)
            align.set_property("right-padding", 1)

        # Use a black border
        edge_window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))

    def make_anchor_win(self, topic_btn, running_btn, edge_gravity):
        # Protect the topic button from the black window bg
        ev_box = gtk.EventBox()
        ev_box.add(topic_btn)
        ev_box.show()

        anchor_win = AnchoredWindow(edge_gravity, running_btn)
        anchor_win.content.pack_start(ev_box, True, True, 0)
        # Use a black border
        anchor_win.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))

        align = anchor_win.get_content_alignment()
        # 1px on either non-edge side
        if edge_gravity in (gtk.gdk.GRAVITY_EAST, gtk.gdk.GRAVITY_WEST):
            if edge_gravity == gtk.gdk.GRAVITY_EAST:
                align.set_property("left-padding", 1)
            elif edge_gravity == gtk.gdk.GRAVITY_WEST:
                align.set_property("right-padding", 1)
            align.set_property("top-padding", 1)
            align.set_property("bottom-padding", 1)
        else:
            if edge_gravity == gtk.gdk.GRAVITY_NORTH:
                align.set_property("bottom-padding", 1)
            elif edge_gravity == gtk.gdk.GRAVITY_SOUTH:
                align.set_property("top-padding", 1)
            align.set_property("left-padding", 1)
            align.set_property("right-padding", 1)

        return anchor_win

    def _drag_data_received(self, widget, context, x, y, selection, target_type, time,
                            running_source):
        running_source.handle_drag_data_received(selection, target_type)
