
import os
import sys, cPickle
import urllib2
from gettext import gettext as _

import gobject
import gtk
import gnomekeyring

from time import strptime,mktime,sleep
from gimmie_base import Item, ItemSource
from gimmie_threads import threadpool, Pool
from gimmie_util import gconf_bridge
from pydelicious import DeliciousAPI, PyDeliciousException

delicious_threads = Pool(4)

picurl = urllib2.urlopen("http://del.icio.us/favicon.ico")
pic = gtk.gdk.PixbufLoader()
pic.write(picurl.read())
pic.close()
icon = pic.get_pixbuf()


class DeliciousLink(Item):    
    def __init__(self, link):
        Item.__init__(self,
                      uri=link['href'],
                      name=link['description'],
                      comment=link['tag'],
                      icon=icon,
                      timestamp=mktime(strptime(link['time'],"%Y-%m-%dT%H:%M:%SZ")),
                      mimetype="text/html")


class DeliciousSource(ItemSource):
    '''
    A list of del.icio.us Links
    '''
    def __init__(self):
        ItemSource.__init__(self,
                            name=_("Delicious Links"),
                            uri="source://delicious",
                            icon=icon,
                            filter_by_date=True)

        delicious_reader.connect("reload", lambda x: self.emit("reload"))
        self.categories = ["Bookmarks"]

    def get_filter_by_date(self):
        return delicious_reader.logged_in == True

    def get_items_uncached(self):
        if not delicious_reader.logged_in:
            yield delicious_reader.get_account_settings()
        else:
            for item in delicious_reader.get_links():
                yield item


class DeliciousAccountReader(gobject.GObject):
    __gsignals__ = {
        "reload" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
    }

    def __init__(self):
        gobject.GObject.__init__(self)

        self.account_list = []

        self.account_settings = DeliciousAccountSettings()

        # Detect when account settings have changed.
        # We need to use two different ways to detect when account settings have
        # changed depending if the gconf key gmail_keyring_token has been set or
        # not. If we only connect to the "reload" signal in GmailAccountSettings
        # there is a race condition reading the gconf key in _login() when the
        # gconf is set for the first time.
        print gconf_bridge.get("delicious_keyring_token")
        if gconf_bridge.get("delicious_keyring_token") > 0:
            self.account_settings.connect("reload", lambda x: self.emit("reload"))
        else:
            gconf_bridge.connect("changed::delicious_keyring_token",
                                 lambda gb: self._gconf_delicious_keyring_token_set())

        self.contacts_path = os.path.expanduser("~/.gimmie/gimmie-delicious-contacts")
        self.logged_in = False

    def _gconf_delicious_keyring_token_set(self):
        if gconf_bridge.get("delicious_keyring_token") > 0:
            self.account_settings.connect("reload", lambda x: self.emit("reload"))
            self.emit("reload")

    def do_reload(self):
        print "reload"
        self._login()

    def _login(self):
        self.account_list = []

        keyring_token = gconf_bridge.get("delicious_keyring_token")

        # Show the del.icio.us Account Setting Window if no account has been set up.
        if keyring_token <= 0:
            self.account_settings.do_open()
            return

        try:
            keyring = gnomekeyring.get_default_keyring_sync()
            passwords = gnomekeyring.item_get_info_sync(keyring,
                                                        keyring_token.get_int()).get_secret()
        except gnomekeyring.Error:
            print " !!! Access to del.icio.us account details denied"
            self.logged_in = False
            return

        accounts = passwords.split(';')

        cancel = False
        for acct in accounts:
            if cancel:
                break
            username, password = acct.split(':')

            # Authenticate using your del.icio.us username and password.
            da = DeliciousAPI(username, password)
            da.File = os.path.expanduser("~/.gimmie/delicious_links_"+username)
            self.account_list.append(da)

        self.logged_in = True

    def get_account_settings(self):
        return self.account_settings

    def get_links(self):
        if self.logged_in == False:
            return []
        pre_links = []

        for acct in self.account_list:
            self.read_links(acct)
            try:
                updated = acct.posts_update()
                if updated["update"] != acct.updated:
                    hlp = acct.posts_all()

                    acct.links = hlp['posts']
                    acct.updated = updated["update"]
                    self.save_links(acct)
            except PyDeliciousException, err:
                print " !!! Del.icio.us error:", err
                self.logged_in = False

            pre_links += acct.links

        links = []
        for link_entry in pre_links:
            link = DeliciousLink(link_entry)
            links.append(link)
        return links

    def save_links(self, acct):
        try:
            os.mkdir(os.path.dirname(acct.File))
        except OSError, err:
            pass
        try:
            f = file(acct.File, "w")
        except IOError, err:
            print err
        else:
            links = {}
            links['posts'] = acct.links
            links['updated'] = acct.updated
            cPickle.dump(links, f)
            f.close()

    def read_links(self, acct):
        try:
            os.mkdir(os.path.dirname(acct.File))
        except OSError, err:
            pass
        try:
            f = file(acct.File, "r")
        except IOError:
            acct.links = {}
            acct.updated = 0
        else:
            links = cPickle.load(f)
            acct.links = links['posts']
            acct.updated = links['updated']
            f.close()


class DeliciousAccountSettings(Item):
    def __init__(self):
        Item.__init__(self,
                      name=_("Login to del.icio.us"),
                      icon="stock_person")

        self.account_win = DeliciousAccountWindow()
        self.account_win.connect("changed", lambda x: self.emit("reload"))
        
    def do_open(self):
        self.account_win.show()
        self.emit("reload")


class DeliciousAccountWindow(gobject.GObject):
    __gsignals__ = {
        "changed" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
    }
    
    def __init__(self):
        gobject.GObject.__init__(self)

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self._construct_ui()
        
        self.window.connect("delete_event", lambda s, w: self.window.destroy())
        
        self.accounts = []
        
    def _construct_ui(self):
        self.window.set_title(_("Login to del.icio.us"))
        self.window.set_border_width(12)

        vbox = gtk.VBox(spacing=18)
        self.window.add(vbox)

        frame = gtk.Frame()
        frame.set_shadow_type(gtk.SHADOW_NONE)
        vbox.pack_start(frame)
        
        label = gtk.Label()
        label.set_markup("<b>" + _("Account Settings") + "</b>")
        frame.set_label_widget(label)
        frame.set_label_align(xalign=0.0, yalign=0.50)
        
        alignment = gtk.Alignment(xalign=0.50, yalign=0.50,
                                  xscale=1.0, yscale=1.0)
        alignment.set_padding(padding_top=5, padding_bottom=0,
                              padding_left=12, padding_right=0)
        frame.add(alignment)
        
        vbox2 = gtk.VBox(spacing=6)
        alignment.add(vbox2)
        
        label = gtk.Label()
        label.set_text(_("Sign in to del.icio.us"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        vbox2.pack_start(label, expand=False)

        # Create table for Username and Pasword entries
        table = gtk.Table(rows=2, columns=2)
        table.set_row_spacings(6)
        table.set_col_spacings(12)
        vbox2.pack_start(table)
        
        label = gtk.Label(_("Username:"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        table.attach(label, left_attach=0, right_attach=1, top_attach=0,
                     bottom_attach=1, xoptions=gtk.FILL, yoptions=0)
        
        label = gtk.Label(_("Password:"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        table.attach(label, left_attach=0, right_attach=1, top_attach=1,
                     bottom_attach=2, xoptions=gtk.FILL, yoptions=0)

        self.username_entry = gtk.Entry()
        table.attach(self.username_entry, left_attach=1, right_attach=2,
                     top_attach=0, bottom_attach=1, yoptions=0)
        
        self.password_entry = gtk.Entry()
        self.password_entry.set_visibility(False)
        self.password_entry.connect("activate", self._button_ok_clicked)
        table.attach(self.password_entry, left_attach=1, right_attach=2,
                     top_attach=1, bottom_attach=2, yoptions=0)
        
        btn_box = gtk.HButtonBox()
        btn_box.set_spacing(6)
        btn_box.set_layout(gtk.BUTTONBOX_END)
        vbox.pack_start(btn_box, expand=False)
        
        btn = gtk.Button(stock=gtk.STOCK_CANCEL)
        btn.connect("clicked", lambda w: self.window.hide())
        btn_box.pack_start(btn)
        
        btn = gtk.Button(stock=gtk.STOCK_OK)
        btn.connect("clicked", self._button_ok_clicked)
        btn_box.pack_start(btn)
        
    def _init_account_settings(self):
        self.keyring = gnomekeyring.get_default_keyring_sync()        

        token = gconf_bridge.get("delicious_keyring_token")
        if not token > 0:
            return

        try:
            passwords = gnomekeyring.item_get_info_sync(self.keyring,
                                                        token.get_int()).get_secret()
        except gnomekeyring.Error:
            return

        try:
            self.accounts = passwords.split(';')

            # Only display first account values
            username, password = self.accounts[0].split(':')
            self.username_entry.set_text(username)
            self.password_entry.set_text(password)
        except ValueError:
            return
        
    def _button_ok_clicked(self, btn):    
        username = self.username_entry.get_text()
        password = self.password_entry.get_text()

        # Replace first account with new the new values.
        secret = [ ":".join((username, password)) ] + self.accounts[1:]

        try:
            token = gnomekeyring.item_create_sync(self.keyring,
                                                  gnomekeyring.ITEM_GENERIC_SECRET,
                                                  "Gimmie del.icio.us account information",
                                                  dict(), ";".join(secret), True)
            gconf_bridge.set("delicious_keyring_token", int(token))
            self.emit("changed")
        except gnomekeyring.Error:
            print " !!! Unable to securely store del.icio.us account details"

        self.window.hide()
        
    def show(self):
        self._init_account_settings()

        self.username_entry.grab_focus()
        self.window.show_all()



#
# Globals
#
delicious_reader = DeliciousAccountReader()
delicious_source = DeliciousSource()

