#!/usr/bin/python

import os
import sys
from gettext import bindtextdomain, textdomain, gettext as _

import awn
import gtk

### Running in panel applet mode.
import gimmie_globals
gimmie_globals.gimmie_is_panel_applet = lambda: True

from gimmie_base import Topic
from gimmie_util import bookmarks, icon_factory, ToolMenuButton, GrabOnShowHelper
from gimmie_topicwin import TopicView, TimeBar


class TopicBox(awn.AppletDialog, TopicView):
    '''
    Panel applet menu version of topic window.
    '''
    def __init__(self, applet, topic):
        awn.AppletDialog.__init__(self, applet)
        TopicView.__init__(self, topic)

        # Tell Awn to draw a pretty titlebar
        self.set_title(topic.get_name())

        self.grab_helper = GrabOnShowHelper(self)

        # Contains the visual frame, giving it some space
        self._content = gtk.VBox(False, 6)
        self._content.set_border_width(0)
        self._content.show()
        self.add(self._content)

        # Toolbar
        self._content.pack_start(self.toolbar, False, False, 0)

        # Hbox containing the sidebar buttons and the toolbar/iconview
        body = gtk.HBox(False, 6)
        body.set_border_width(0)
        body.show()
        self._content.pack_start(body, True, True, 0)

        # Load up the sidebar
        self._sidebar_align = gtk.Alignment(0.5, 0.0, 0.0, 0.0)
        self._sidebar_align.add(self.sidebar)
        self._sidebar_align.show()
        body.pack_start(self._sidebar_align, False, True, 0)

        # Horizontal time line bar with plus/minus buttons above icon view
        self.timebar = TimeBar()
        self.timebar.connect("zoom-changed", lambda w, num_days: self.zoom_changed(num_days))
        align = gtk.Alignment(1.0, 0.0, 0.0, 0.0)
        align.add(self.timebar)
        align.show()
        evbox = gtk.EventBox()
        evbox.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        evbox.show()
        timebox = gtk.HBox(False, 0)
        timebox.pack_start(evbox, True, True)
        timebox.pack_start(align, False, True)
        timebox.show()
        self.content_vbox.pack_start(timebox, False, False)

        # Frame containing timebar and iconview inside scrolled window
        body.pack_start(self.content_frame, True, True, 0)

        # Bound the height request of the scrolled window and icon view
        #self.scroll.connect("size-request", self._scroll_get_best_size)
        self.scroll.get_vadjustment().connect("changed",
                                              lambda adj: self.scroll.queue_resize_no_redraw())

        self.view.set_size_request(500, -1)
        self.view.connect("item-activated", lambda v, p: self.hide()) # Iconify on item open

        # Setup the toolbar
        self._add_toolbar_items()

        # Select an initial sidebar button
        self.find_first_button()

    def set_gravity(self, gravity):
        # Override GtkWindow::set_gravity
        assert self.toolbar.parent == self._content

        self._content.remove(self.toolbar)
        if gravity in (gtk.gdk.GRAVITY_SOUTH,
                       gtk.gdk.GRAVITY_SOUTH_WEST,
                       gtk.gdk.GRAVITY_SOUTH_EAST):
            # Toolbar & sidebar at the bottom
            self._content.pack_end(self.toolbar, False, False, 0)
            self._sidebar_align.set_property("yalign", 1.0)
        else:
            # Toolbar & sidebar at the top
            self._content.pack_start(self.toolbar, False, False, 0)
            self._sidebar_align.set_property("yalign", 0.0)

        gtk.Window.set_gravity(self, gravity)

    def get_zoom_level(self):
        return self.timebar.get_zoom_level()

    def get_zoom_level_list(self):
        return self.timebar.get_zoom_level_list()

    def set_zoom_level(self, zoom):
        self.timebar.set_zoom_level(zoom)

    def show_hide_zoomer(self, show):
        if show:
            if self.active_source:
                self.timebar.set_items(self.active_source.get_items())
            self.timebar.show()
        else:
            self.timebar.hide()

    def _scroll_get_best_size(self, scroll, req):
        '''
        Sets the height request of the scrolled window to either the upper bound
        of the scrollbar or 70% of the screen height, whichever is smaller.
        '''
        vadj = scroll.get_property("vadjustment")
        upper = int(vadj.get_property("upper"))
        upper = min(upper, int(self.get_screen().get_height() * 0.7))

        if upper == 0 or upper == scroll.allocation.height:
            # Avoid unneeded or bogus resize
            return False

        req.height = upper
        return True

    def _load_items_done(self, ondone_cb):
        '''
        Do some magic to get a decent size request out of an iconview.  Reset
        the scrolled window upper bound to 0, and resize the window.
        '''
        if ondone_cb:
            ondone_cb()

        vadj = self.scroll.get_property("vadjustment")
        vadj.set_property("upper", 0)

        ### NOTE: Awn applet isn't a toplevel, it's embedded in an AppletDialog
        #w, h = self.child.size_request()
        #self.resize(w, h)

    def load_items(self, items, ondone_cb = None):
        '''
        Wrap the ondone callback to resize the scrolled window after reloading
        the iconview.
        '''
        self.view.load_items(items, lambda: self._load_items_done(ondone_cb))

    def _favorite_selection_changed(self, view, fav, block_id, img):
        # Block the toggled handler from triggering
        fav.handler_block(block_id)
        selected = view.get_selected_items()
        if len(selected) == 1:
            model = view.get_model()
            item = model.get_value(model.get_iter(selected[0]), 2)

            pix = icon_factory.load_icon("emblem-favorite", gtk.ICON_SIZE_LARGE_TOOLBAR)
            if not item.get_is_pinned():
               pix = icon_factory.greyscale(pix)
            img.set_from_pixbuf(pix)

            fav.set_active(item.get_is_pinned())
            fav.set_sensitive(item.get_can_pin())
        else:
            fav.set_active(False)
            fav.set_sensitive(False)
        fav.handler_unblock(block_id)

    def _favorite_toggled(self, fav):
        selected = self.view.get_selected_items()
        if len(selected) == 1:
            model = self.view.get_model()
            item = model.get_value(model.get_iter(selected[0]), 2)
            if fav.get_active():
                item.pin()
            else:
                item.unpin()

    def add_favorite_toolitem(self):
        img = icon_factory.load_image("emblem-favorite", gtk.ICON_SIZE_LARGE_TOOLBAR)
        
        i = gtk.ToggleToolButton()
        i.set_label(_("Favorite"))
        i.set_icon_widget(img)
        i.set_is_important(True)
        i.set_sensitive(False)
        i.set_tooltip(self.tooltips, _("Add to Favorites"))
        i.show_all()
        self.toolbar.insert(i, -1)
        
        block_id = i.connect("toggled", self._favorite_toggled)
        
        sel_changed_cb = lambda: self._favorite_selection_changed(self.view, i, block_id, img)
        self.view.connect("selection-changed", lambda view: sel_changed_cb())
        bookmarks.connect("reload", lambda b: sel_changed_cb())

        self.fav = i

    def _make_toolbar_expander(self, expand = True):
        sep = gtk.SeparatorToolItem()
        sep.set_draw(False)
        sep.set_expand(expand)
        sep.show()
        return sep

    def _add_toolbar_items(self):
        # Right-align the zoom and search tool items
        self.toolbar.insert(self._make_toolbar_expander(), -1)

        for i in self.topic.get_toolbar_items(self.tooltips):
            ### Uncomment to pack space between elements
            #self.toolbar.insert(self._make_toolbar_expander(), -1)
            if i:
                i.show_all()
                if isinstance(i, ToolMenuButton):
                    for menu_item in i.get_menu() or []:
                        menu_item.connect("activate", lambda menu_item: self.hide())
                elif isinstance(i, gtk.ToolButton):
                    i.connect("clicked", lambda btn: self.hide())
                self.toolbar.insert(i, -1)

        self.add_favorite_toolitem()

        # Try to disassociate the Favorite button from the search box
        self.toolbar.insert(self._make_toolbar_expander(False), -1)

        self.add_search_toolitem()

        # Right-align the zoom and search tool items
        self.toolbar.insert(self._make_toolbar_expander(), -1)


class GimmieAwnApplet(awn.AppletSimple):
    def __init__ (self, uid, orient, height, topic):
        awn.AppletSimple.__init__ (self, uid, orient, height)
        self.topic = topic
        self.topic_win = None

        self.connect("height_changed", self.height_changed)
        self.height_changed(height)

        self.title = awn.awn_title_get_default()
        self.connect("enter-notify-event", self.enter_notify)
        self.connect("leave-notify-event", self.leave_notify)

        self.connect("button-press-event", self.button_press)

    def height_changed(self, height):
        self.set_icon(self.topic.get_icon(height))

    def button_press(self, widget, event):
        if not self.topic_win:
            self.topic_win = TopicBox(self, self.topic)
            self.topic.set_topic_window(self.topic_win)
            if self.get_orientation() == 0: # bottom
                self.topic_win.set_gravity(gtk.gdk.GRAVITY_SOUTH)

        if self.topic_win.get_property("visible"):
            self.topic_win.hide()
        else:
            self.title.hide(self)
            self.topic_win.search_tool_item.entry.grab_focus()
            self.topic_win.show_all()

    def enter_notify(self, widget, event):
        self.title.show(self, self.topic.get_name())

    def leave_notify(self, widget, event):
        self.title.hide(self)


def start(topic):
    assert topic, "Invalid topic!"
    
    print "%s: Starting pid=%s args=%s" % (sys.argv[0], os.getpid(), sys.argv[1:])

    bindtextdomain('gimmie', gimmie_globals.localedir)
    textdomain('gimmie')

    # Tell gobject/gtk we are threaded
    gtk.gdk.threads_init()

    awn.init(sys.argv[1:])

    applet = GimmieAwnApplet(awn.uid, awn.orient, awn.height, topic)
    awn.init_applet(applet)
    applet.show_all()

    gtk.main()


if __name__ == "__main__":
    print "Just for testing!  This shouldn't be invoked directly!"
    try:
        from gimmie_applications import ApplicationsTopic
    except:
        from gimmie.gimmie_applications import ApplicationsTopic
    start(ApplicationsTopic())
