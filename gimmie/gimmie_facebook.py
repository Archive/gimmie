#!/usr/bin/env python

import os
import sys
import urllib2
import time
from gettext import gettext as _

import gtk
import pango
import gobject
import gtkmozembed

from gimmie_base import Item, ItemSource
from gimmie_threads import threadpool, Pool

from facebook import Facebook, FacebookError


facebook_threads = Pool(4)


class FacebookAccountReader(gobject.GObject):
    __gsignals__ = {
        "reload" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
        }

    ### API Keys for "Gimmie Desktop Organizer"
    API_KEY = '833782108d0a0626157feedb2ef66106'
    SECRET_KEY = 'e07e5c6851b7c862604b11e7ab0f943b'

    def __init__(self):
        gobject.GObject.__init__(self)

        self.logged_in = False
        self.friends = []
        self.session_key_file = os.path.expanduser("~/.gimmie-facebook")
        self.needs_auth = False

        self.__fb = Facebook(self.API_KEY, self.SECRET_KEY)
        self.load_existing_auth()

    @threadpool(facebook_threads)
    def load_existing_auth(self):
        try:
            f = open(self.session_key_file, 'r')
            self.__fb.session_key = f.readline().strip()
            self.__fb.secret = f.readline().strip()
            self.__fb.uid = f.readline().strip()
            f.close()

            print 'Facebook: Using existing Session Key ', self.__fb.session_key
            self.needs_auth = False

            # Kick off threaded friend loading
            self.load_friends()
        except FacebookError, e:
            print "Facebook: Invalid existing Session Key: ", e
            self.__fb.session_key = None
            self.__fb.secret = None
            self.__fb.uid = None
            self.__fb.auth.createToken()
            self.needs_auth = True
        except IOError, e:
            print "Facebook: No existing Session Key"
            self.__fb.auth.createToken()
            self.needs_auth = True

        gtk.gdk.threads_enter()
        self.emit("reload")
        gtk.gdk.threads_leave()

    @threadpool(facebook_threads)
    def finish_auth(self):
        try:
            self.__fb.auth.getSession()

            f = open(self.session_key_file, 'w')
            f.write(self.__fb.session_key + "\n")
            f.write(self.__fb.secret + "\n")
            f.write(self.__fb.uid + "\n")
            f.close()

            print 'Facebook: New Session Key: ', self.__fb.session_key
            self.needs_auth = False

            # Kick off threaded friend loading
            self.load_friends()
        except FacebookError, e:
            print "Facebook: Error auto-authenticating session: ", e
            self.__fb.auth.createToken()
            self.needs_auth = True

        gtk.gdk.threads_enter()
        self.emit("reload")
        gtk.gdk.threads_leave()

    @threadpool(facebook_threads)
    def load_friend_items(self, friend_uids):
        try:
            friend_infos = self.__fb.users.getInfo(friend_uids,
                                                   ['name',
                                                    'birthday',
                                                    'uid',
                                                    'pic_small',
                                                    'status'])
        except FacebookError, e:
            print "Facebook: Fetching friend info failed:", e
            return

        items = []

        for friend in friend_infos:
            icon = "stock_person"
            icon_url = friend['pic_small']
            if icon_url:
                try:
                    picurl = urllib2.urlopen(friend['pic_small'])

                    pic = gtk.gdk.PixbufLoader()
                    pic.write(picurl.read())
                    pic.close()
                
                    icon = pic.get_pixbuf()
                except (IOError, LookupError):
                    pass

            message = _("Unknown")
            timestamp = 0
            status = friend['status']
            if status:
                try:
                    message = status['message']
                except IOError:
                    pass

            item = FacebookFriend(friend['name'],
                                  str(friend['uid']),
                                  icon,
                                  message)
            items.append(item)

        gtk.gdk.threads_enter()
        self.friends += items
        self.emit("reload")
        print "Facebook: Loaded info for %d friends." % len(items)
        gtk.gdk.threads_leave()

    def load_friends(self):
        # This throws FacebookError if the session key is invalid
        friend_uids = self.__fb.friends.get()

        # Reset friends
        self.friends = []

        # Load 10 friends at a time in a worker thread
        for i in range(len(friend_uids) / 10 + 1):
            self.load_friend_items(friend_uids[i*10:i*10+10])

    def get_needs_auth(self):
        return self.needs_auth

    def get_friends(self):
        for f in self.friends:
            yield f

    def get_login_url(self):
        return self.__fb.get_login_url(popup=True)

    def get_auth_complete_url(self):
        return "http://www.facebook.com/desktopapp.php?api_key=" + self.API_KEY + "&popup"


class FacebookFriend(Item):
    FRIEND_PROFILE_URI = "http://www.facebook.com/profile.php?id="
    
    def __init__(self, name, uid, icon, status):
        Item.__init__(self,
                      uri=self.FRIEND_PROFILE_URI + uid,
                      name=name,
                      comment=status,
                      icon=icon,
                      mimetype="facebook/contact")
        self.uid = uid


class FacebookLogin(Item):
    def __init__(self, icon):
        Item.__init__(self,
                      name=_("Login to Facebook"),
                      comment=_("Access your Facebook friends"),
                      icon=icon,
                      mimetype="facebook/login")

    def get_profile_path(self):
        return os.path.expanduser("~/.mozilla/gimmie")

    def create_prefs_js(self):
        '''
        Create the file prefs.js in the mozilla profile directory.  This file
        does things like turn off the warning when navigating to https pages.
        '''

        prefsContent = '''\
 	# Mozilla User Preferences
 	user_pref("security.warn_entering_secure", false);
 	user_pref("security.warn_leaving_secure", false);
 	user_pref("security.warn_leaving_secure.show_once", false);
        user_pref("signon.rememberSignons", true);
        
        '''

        prefsPath = os.path.join(self.get_profile_path(), 'default', 'prefs.js')
        if not os.path.exists(prefsPath):
            f = open(prefsPath, "wt")
            f.write(prefsContent)
            f.close()

    def create_profile_dirs(self):
        path = os.path.join(self.get_profile_path(), 'default')
 	if not os.path.exists(path):
 	    os.makedirs(path)

    def init_gtkmozembed(self):
        self.create_profile_dirs()
        self.create_prefs_js()
        gtkmozembed.set_profile_path(self.get_profile_path(), "default")

    def new_location(self, moz, mozwin):
        if moz.get_location() == facebook_reader.get_auth_complete_url():
            print "Facebook: completing authentication..."
            gobject.idle_add(lambda: mozwin.destroy() and False)

    def finish_auth_on_close(self):
        facebook_reader.finish_auth()

    def do_open(self):
        self.init_gtkmozembed()
        
        moz = gtkmozembed.MozEmbed()
        moz.load_url(facebook_reader.get_login_url())
        moz.show()

        mozwin = gtk.Window(gtk.WINDOW_TOPLEVEL)
        mozwin.set_title(_("Login to Facebook"));
        mozwin.set_default_size(540, 520)
        mozwin.add(moz)
        mozwin.show()

        moz.connect("location", lambda w, mozwin: self.new_location(w, mozwin), mozwin)
        mozwin.connect("destroy", lambda w: self.finish_auth_on_close())


class FacebookSource (ItemSource):   
    def __init__(self, name):
        ItemSource.__init__(self,
                            name=name,
                            uri="source://People/Facebook",
                            icon="stock_people",
                            filter_by_date=False)

        facebook_reader.connect("reload", lambda x: self.emit("reload"))
        self.load_icon()

    @threadpool(facebook_threads)
    def load_icon(self):
        try:
            picurl = urllib2.urlopen("http://www.facebook.com/favicon.ico")
            pic = gtk.gdk.PixbufLoader()
            pic.write(picurl.read())
            pic.close()

            gtk.gdk.threads_enter()
            self.icon = pic.get_pixbuf()
            self.emit("reload")
            gtk.gdk.threads_leave()
        except IOError:
            pass

    def get_items_uncached(self):
        if facebook_reader.get_needs_auth():
            yield FacebookLogin(self.icon)
        else:
            for item in facebook_reader.get_friends():
                yield item


facebook_reader = FacebookAccountReader()
