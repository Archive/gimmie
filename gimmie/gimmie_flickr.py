#!/usr/bin/python

import os
import sys
import urllib2
import time
import thread
from gettext import gettext as _

import gtk
import pango
import gobject
import gtkmozembed

from gimmie_base import Item, ItemSource
from gimmie_threads import threadpool, Pool
from gimmie_util import icon_factory

import flickrapi


flickr_threads = Pool(4)


class FlickrAccountReader(gobject.GObject):
    __gsignals__ = {
        "reload" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
        }

    ### API Keys for "Gimmie Desktop Organizer"
    API_KEY = 'a9524d11ce88009e9194d504bba381c6'
    API_SECRET = 'fcf75bed55c95f0b'
   
    def __init__(self):
        gobject.GObject.__init__(self)

        self.needs_auth = False
        self.auth_token = None
        self.auth_frob = None
        self.uid = None
        self.photos = []
        self.friends = []
        
        self.flickr = flickrapi.FlickrAPI(self.API_KEY, self.API_SECRET)
        self.load_existing_auth()

    @threadpool(flickr_threads)
    def load_existing_auth(self):
        (self.auth_token, self.auth_frob) = self.flickr.getTokenPartOne(perms='write')

        if self.auth_token:
            self.finish_auth()
        else:
            gtk.gdk.threads_enter()
            self.needs_auth = True
            self.emit("reload")
            gtk.gdk.threads_leave()

    @threadpool(flickr_threads)
    def finish_auth(self):
        self.flickr.getTokenPartTwo((self.auth_token, self.auth_frob))

        sets = self.flickr.test_login()
        self.uid = sets.user[0]['id']
        self.needs_auth = False

        self.load_photos()

    def load_photos(self):
        sets = self.flickr.people_getPublicPhotos(user_id=self.uid, extras='tags')
        photos = []

        for photo in sets.photos[0].photo:
            photos.append(FlickrPicture(photo))
        
        for photo in self.get_friends_photos():
            photos.append(photo)
        
        gtk.gdk.threads_enter()
        self.photos = photos 
        self.emit("reload")
        gtk.gdk.threads_leave()
        
    def get_id(self):
        return self.uid

    def get_friends_photos(self):
        
        self.get_friends()
        photos=[]
        for friend in self.friends:
            sets = self.flickr.people_getPublicPhotos(user_id=friend.id, extras='tags')
            for photo in sets.photos[0].photo:
                photos.append(FlickrPicture(photo))
        return photos    
        
    def get_friends(self):
        sets = self.flickr.contacts_getList()
        friends = []

        for friend in sets.contacts[0].contact:
            self.friends.append(FlickrFriend(friend))

    def get_my_photos(self):
        return self.photos

    def get_needs_auth(self):
        return self.needs_auth

    def get_login_url(self):
        return self.flickr.getAuthURL(perms="write", frob=self.auth_frob)

    def get_auth_complete_url(self):
        return None


class FlickrPicture(Item):  
    def __init__(self, photo):
        self.farm = photo['farm']
        self.pid = photo['id']
        self.server = photo['server']
        self.secret = photo['secret']
        #self.format = photo['original_format']
        self.uid = photo['owner']

        picurl = urllib2.urlopen("http://farm" + self.farm + ".static.flickr.com/" + self.server + "/" + self.pid +"_" + self.secret + "_s.jpg")
        pic = gtk.gdk.PixbufLoader()
        pic.write(picurl.read())
        pic.close()

        icon = pic.get_pixbuf()
        icon = icon_factory.make_icon_frame(icon)
        
        Item.__init__(self,
                      uri="http://www.flickr.com/photos/"+ self.uid + "/" + self.pid,
                      name=photo['title'],
                      mimetype="flickr/picture",
                      icon=icon,
                      comment=_("Tags: ") + photo['tags'])

    def build_photo_uri(self, size):
        if size in ['s','t']:
            return "http://farm" + self.farm + ".static.flickr.com/" + self.server + "/" + image.id +"_" + self.secret + "_" + size + ".jpg"
        #elif size == "o":
            #return "http://farm" + self.farm + ".static.flickr.com/" + self.server + "/" + image.id +"_" + self.secret + "_" + size + self.format
        return None


class FlickrFriend(gobject.GObject):
    def __init__(self, node):
        self.id = node['nsid']
        self.username = node['username']
        self.realname = node['realname']
        self.iconserver = node['iconserver']
        self.friend = node['friend']
        self.family = node['family']
        #self.ignore = node['ignore']
        
        
class FlickrLogin(Item):
    def __init__(self, icon):
        Item.__init__(self,
                      name=_("Login to Flickr"),
                      comment=_("Access your Flickr photos"),
                      icon=icon,
                      mimetype="flickr/login")

    def get_profile_path(self):
        return os.path.expanduser("~/.mozilla/gimmie")

    def create_prefs_js(self):
        '''
        Create the file prefs.js in the mozilla profile directory.  This file
        does things like turn off the warning when navigating to https pages.
        '''

        prefsContent = '''\
 	# Mozilla User Preferences
 	user_pref("security.warn_entering_secure", false);
 	user_pref("security.warn_leaving_secure", false);
 	user_pref("security.warn_leaving_secure.show_once", false);
        user_pref("signon.rememberSignons", true);
        
        '''

        prefsPath = os.path.join(self.get_profile_path(), 'default', 'prefs.js')
        if not os.path.exists(prefsPath):
            f = open(prefsPath, "wt")
            f.write(prefsContent)
            f.close()

    def create_profile_dirs(self):
        path = os.path.join(self.get_profile_path(), 'default')
 	if not os.path.exists(path):
 	    os.makedirs(path)

    def init_gtkmozembed(self):
        self.create_profile_dirs()
        self.create_prefs_js()
        gtkmozembed.set_profile_path(self.get_profile_path(), "default")

    def new_location(self, moz, mozwin):
        if moz.get_location() == flickr_reader.get_auth_complete_url():
            print "Flickr: completing authentication..."
            gobject.idle_add(lambda: mozwin.destroy() and False)

    def finish_auth_on_close(self):
        flickr_reader.finish_auth()

    def do_open(self):
        self.init_gtkmozembed()
        
        moz = gtkmozembed.MozEmbed()
        moz.load_url(flickr_reader.get_login_url())
        moz.show()

        mozwin = gtk.Window(gtk.WINDOW_TOPLEVEL)
        mozwin.set_title(_("Login to Flickr"));
        mozwin.set_default_size(840, 560)
        mozwin.add(moz)
        mozwin.show()

        moz.connect("location", lambda w, mozwin: self.new_location(w, mozwin), mozwin)
        mozwin.connect("destroy", lambda w: self.finish_auth_on_close())


class FlickrSource(ItemSource):  
    def __init__(self):
        ItemSource.__init__(self,
                            name=_("Flickr Photos"),
                            uri="source://Library/FlickrPhotos",
                            filter_by_date=False)

        flickr_reader.connect("reload", lambda x: self.emit("reload"))
        self.load_icon()

    @threadpool(flickr_threads)
    def load_icon(self):
        try:
            picurl = urllib2.urlopen("http://www.flickr.com/favicon.ico")
            pic = gtk.gdk.PixbufLoader()
            pic.write(picurl.read())
            pic.close()

            gtk.gdk.threads_enter()
            self.icon = pic.get_pixbuf()
            self.emit("reload")
            gtk.gdk.threads_leave()
        except IOError:
            pass

    def get_items_uncached(self):
        if flickr_reader.get_needs_auth():
            yield FlickrLogin(self.icon)
        else:
            for item in flickr_reader.get_my_photos():
                yield item


flickr_reader = FlickrAccountReader()
