
import os
import gobject

import gtk
from gimmie_base import Item, ItemSource

import evolution


class EvolutionContactWindow(gtk.Window):
    def __init__(self,contact):
        
        gtk.Window.__init__(self)
        
        table = gtk.Table(2, 2, True)
        self.add(table)
        
        label= gtk.Label(contact.get_name())
        table.attach(label, 0, 1, 0, 1)
        
        self.combobox = gtk.combo_box_new_text()
        table.attach(self.combobox, 1, 2, 0, 1)
        
        button = gtk.Button("Compose")
        gtk.gdk.threads_init()
        gtk.gdk.threads_enter()
        button.connect("clicked",self.compose)
        gtk.gdk.threads_leave()
        table.attach(button, 1, 2, 1, 2)
        
        mail=str(contact.get_property('email_1'))
        if(mail!="None"):
           self.combobox.append_text(mail)    
        mail=str(contact.get_property('email_2'))
        if(mail!="None"):
            self.combobox.append_text(mail) 
        mail=str(contact.get_property('email_3'))
        if(mail!="None"):
            self.combobox.append_text(mail) 
        mail=str(contact.get_property('email_4'))
        if(mail!="None"):
            self.combobox.append_text(mail) 
        
        self.combobox.set_active(0)
        gtk.gdk.threads_init()
        gtk.gdk.threads_enter()
        self.show_all()
        gtk.gdk.threads_leave()
    
    def get_active_text(combobox):
      model = self.combobox.get_model()
      active = self.combobox.get_active()
      if active < 0:
          return ""
      return model[active][0]
    
    def compose(self,temp):
        #self.destroy()
        self.destroy()
        entry = self.combobox.get_active_text()
        os.system('evolution mailto:'+entry)


class EvolutionContact(Item):
    def __init__(self,c):
        self.contact=c
        Item.__init__(self,
                      uri='mailto:'+str(c.get_property('full-name')),
                      name=c.get_property('full-name'),
                      comment=c.get_property('email_1'),
                      icon="stock_person",
                      mimetype="gmail/contact")
        print(str(c.get_name()))
        
    def do_open(self):
        #os.system('evolution mailto:'+self.contact.get_property('email_1'))
        ecw = EvolutionContactWindow(self.contact)
        ecw.show()


class EvolutionSource(ItemSource):
    def __init__(self):
        ItemSource.__init__(self,
                            name="Evolution",
                            icon="stock_mail",
                            uri="source://People/Evolution",
                            filter_by_date=False)
        self.categories = ["Evolution"]
        self.topics = ["People"]

    def _get_all_contacts(self):
        contacts = []
        for book_id in evolution.ebook.list_addressbooks():
            book = evolution.ebook.open_addressbook(book_id[1])
            if book:
                for c in book.get_all_contacts():
                    contact = EvolutionContact(c)
                    contacts.append(contact)
        return contacts

    def get_items_uncached(self):
        return self._get_all_contacts()
