import os
import sys
import urllib

from gettext import gettext as _

import gobject
import gtk
import gnomekeyring

import libgmail

from gimmie_base import Item, ItemSource
from gimmie_util import gconf_bridge


class GmailAccountWindow(gobject.GObject):
    __gsignals__ = {
        "changed" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
    }
    
    def __init__(self):
        gobject.GObject.__init__(self)

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self._construct_ui()
        
        self.window.connect("delete_event", lambda s, w: self.window.destroy())
        
        self.accounts = []
        
    def _construct_ui(self):
        self.window.set_title(_("Login to Gmail"))
        self.window.set_border_width(12)

        vbox = gtk.VBox(spacing=18)
        self.window.add(vbox)

        frame = gtk.Frame()
        frame.set_shadow_type(gtk.SHADOW_NONE)
        vbox.pack_start(frame)
        
        label = gtk.Label()
        label.set_markup("<b>" + _("Account Settings") + "</b>")
        frame.set_label_widget(label)
        frame.set_label_align(xalign=0.0, yalign=0.50)
        
        alignment = gtk.Alignment(xalign=0.50, yalign=0.50,
                                  xscale=1.0, yscale=1.0)
        alignment.set_padding(padding_top=5, padding_bottom=0,
                              padding_left=12, padding_right=0)
        frame.add(alignment)
        
        vbox2 = gtk.VBox(spacing=6)
        alignment.add(vbox2)
        
        label = gtk.Label()
        label.set_text(_("Sign in to Gmail with your Google Account"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        vbox2.pack_start(label, expand=False)

        # Create table for Username and Pasword entries
        table = gtk.Table(rows=2, columns=2)
        table.set_row_spacings(6)
        table.set_col_spacings(12)
        vbox2.pack_start(table)
        
        label = gtk.Label(_("Username:"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        table.attach(label, left_attach=0, right_attach=1, top_attach=0,
                     bottom_attach=1, xoptions=gtk.FILL, yoptions=0)
        
        label = gtk.Label(_("Password:"))
        label.set_alignment(xalign=0.0, yalign=0.50)
        table.attach(label, left_attach=0, right_attach=1, top_attach=1,
                     bottom_attach=2, xoptions=gtk.FILL, yoptions=0)

        self.username_entry = gtk.Entry()
        table.attach(self.username_entry, left_attach=1, right_attach=2,
                     top_attach=0, bottom_attach=1, yoptions=0)
        
        self.password_entry = gtk.Entry()
        self.password_entry.set_visibility(False)
        self.password_entry.connect("activate", self._button_ok_clicked)
        table.attach(self.password_entry, left_attach=1, right_attach=2,
                     top_attach=1, bottom_attach=2, yoptions=0)
        
        btn_box = gtk.HButtonBox()
        btn_box.set_spacing(6)
        btn_box.set_layout(gtk.BUTTONBOX_END)
        vbox.pack_start(btn_box, expand=False)
        
        btn = gtk.Button(stock=gtk.STOCK_CANCEL)
        btn.connect("clicked", lambda w: self.window.hide())
        btn_box.pack_start(btn)
        
        btn = gtk.Button(stock=gtk.STOCK_OK)
        btn.connect("clicked", self._button_ok_clicked)
        btn_box.pack_start(btn)
        
    def _init_account_settings(self):
        self.keyring = gnomekeyring.get_default_keyring_sync()        

        token = gconf_bridge.get("gmail_keyring_token")
        if not token > 0:
            return

        try:
            passwords = gnomekeyring.item_get_info_sync(self.keyring, token).get_secret()
        except gnomekeyring.Error:
            return

        try:
            self.accounts = passwords.split(';')

            # Only display first account values
            username, password = self.accounts[0].split(':')
            self.username_entry.set_text(username)
            self.password_entry.set_text(password)
        except ValueError:
            return
        
    def _button_ok_clicked(self, btn):    
        username = self.username_entry.get_text()
        password = self.password_entry.get_text()

        # Replace first account with new the new values.
        secret = [ ":".join((username, password)) ] + self.accounts[1:]

        try:
            token = gnomekeyring.item_create_sync(self.keyring,
                                                  gnomekeyring.ITEM_GENERIC_SECRET,
                                                  "Gimmie Gmail account information",
                                                  dict(), ";".join(secret), True)
            gconf_bridge.set("gmail_keyring_token", int(token))
            self.emit("changed")
        except gnomekeyring.Error:
            print " !!! Unable to securely store GMail account details"

        self.window.hide()
        
    def show(self):
        self._init_account_settings()

        self.username_entry.grab_focus()
        self.window.show_all()


class GmailAccountSettings(Item):
    def __init__(self):
        Item.__init__(self,
                      name=_("Login to Gmail"),
                      icon="stock_person")

        self.account_win = GmailAccountWindow()
        self.account_win.connect("changed", lambda x: self.emit("reload"))
        
    def do_open(self):
        self.account_win.show()


class GmailContact(Item):
    _GMAIL_COMPOSE_URI = "http://mail.google.com/mail/?&view=cm&to=%s"
    
    def __init__(self, name, email):
        Item.__init__(self,
                      uri=GmailContact._GMAIL_COMPOSE_URI % urllib.quote(email),
                      name=name,
                      comment=email,
                      icon="stock_mail",
                      mimetype="gmail/contact")
        self.email = email


class GmailSource(ItemSource):
    '''
    A list of Gmail contacts
    '''
    GMAIL_CONTACTS_UPDATE_INTERVAL = 3600000 # 1 hour in milliseconds
    
    def __init__(self, name):
        ItemSource.__init__(self,
                            name=name,
                            icon="stock_mail",
                            uri="source://People/Gmail",
                            filter_by_date=False)

        gmail_reader.connect("reload", lambda x: self.emit("reload"))

        # Set up timer to update contacts at regular intervals.
        gobject.timeout_add(GmailSource.GMAIL_CONTACTS_UPDATE_INTERVAL,
                            lambda: self.emit("reload") and True)

    def get_items_uncached(self):
        yield gmail_reader.get_account_settings()

        for item in gmail_reader.get_contacts():
            yield item


class GmailAccountReader(gobject.GObject):
    __gsignals__ = {
        "reload" : (gobject.SIGNAL_RUN_FIRST,
                    gobject.TYPE_NONE,
                    ())
    }

    def __init__(self):
        gobject.GObject.__init__(self)

        self.account_list = []

        self.account_settings = GmailAccountSettings()

        # Detect when account settings have changed.
        # We need to use two different ways to detect when account settings have
        # changed depending if the gconf key gmail_keyring_token has been set or
        # not. If we only connect to the "reload" signal in GmailAccountSettings
        # there is a race condition reading the gconf key in _login() when the
        # gconf is set for the first time.
        if gconf_bridge.get("gmail_keyring_token") > 0:
            self.account_settings.connect("reload", lambda x: self.emit("reload"))
        else:
            gconf_bridge.connect("changed::gmail_keyring_token",
                                 lambda gb: self._gconf_gmail_keyring_token_set())

        self.contacts_path = os.path.expanduser("~/.gimmie-contacts")

        self.logged_in = False

    def _gconf_gmail_keyring_token_set(self):
        if gconf_bridge.get("gmail_keyring_token") > 0:
            self.account_settings.connect("reload", lambda x: self.emit("reload"))
            self.emit("reload")

    def do_reload(self):
        self._login()

    def _login(self):
        self.account_list = []

        keyring_token = gconf_bridge.get("gmail_keyring_token")

        # Show the Gmail Account Setting Window if no account has been set up.
        if keyring_token <= 0:
            self.account_settings.do_open()
            return

        try:
            keyring = gnomekeyring.get_default_keyring_sync()
            passwords = gnomekeyring.item_get_info_sync(keyring, keyring_token).get_secret()
        except gnomekeyring.Error:
            print " !!! Access to GMail account details denied"
            self.logged_in = False
            return

        accounts = passwords.split(';')

        cancel = False
        for acct in accounts:
            if cancel:
                break
            username, password = acct.split(':')

            successful = False
            while not successful:
                try:
                    ga = libgmail.GmailAccount(username, password)
                    ga.login()
                    successful = True
                    self.account_list.append(ga)
                except libgmail.GmailLoginFailure, err:
                    msg_text = _('Login failed for %s:\n%s') % (username, err)
                    dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, message_format=msg_text)

                    if len(accounts) > 1:
                        dialog.add_button( _('_Ignore'), gtk.RESPONSE_REJECT)
                    dialog.add_button( gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
                    dialog.add_button(_('_Retry'), gtk.RESPONSE_ACCEPT)
                    dialog.set_default_response(gtk.RESPONSE_ACCEPT)
                    response = dialog.run()
                    dialog.destroy()

                    # Skip this account and go to next account
                    if response == gtk.RESPONSE_REJECT:
                        break

                    if response == gtk.RESPONSE_CANCEL or \
                       response == gtk.RESPONSE_DELETE_EVENT:
                        cancel = True
                        break

                    # Try logging into the account again
                    continue

        self.logged_in = True

    def get_account_settings(self):
        return self.account_settings

    def get_contacts(self):
        if self.logged_in == False:
            return []

        contact_items = []

        if len(self.account_list) > 0:
            f = file(self.contacts_path, "w")

            for acct in self.account_list:
                contacts = acct.getContacts()
                for contact in contacts.getAllContacts():
                    item = GmailContact(contact.getName(), contact.getEmail())
                    contact_items.append(item)
                    f.write(contact.getName() + ":" + contact.getEmail() + "\n")

        else:
            try:
                f = file(self.contacts_path, "r")
            except IOError:
                return []

            for contact in f.readlines():
                name, email = contact.split(":")
                item = GmailContact(name, email)
                contact_items.append(item)

        f.close()

        return contact_items


#
# Globals
#

gmail_reader = GmailAccountReader()
