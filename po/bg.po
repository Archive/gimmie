# Bulgarian translation of gimmie po-file.
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the gimmie package.
# Alexander Shopov <ash@contact.bg>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: gimmie trunk\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-11-28 07:25+0200\n"
"PO-Revision-Date: 2007-11-28 08:06+0200\n"
"Last-Translator: Alexander Shopov <ash@contact.bg>\n"
"Language-Team: Bulgarian <dict@fsa-bg.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../data/gimmie.schemas.in.h:1
msgid "Applet button style"
msgstr "Стил на бутоните на аплета"

#: ../data/gimmie.schemas.in.h:2
msgid "Click policy"
msgstr "Политика на натискането"

#: ../data/gimmie.schemas.in.h:3
msgid ""
"Determine how to handle clicks on items in Gimmie. The following options are "
"supported: \"single\" activates items with a single click, \"double\" "
"activates with a double click, and \"nautilus\" to follow Nautilus' click "
"policy."
msgstr ""
"Задаване как да се обработват натисканията с мишката върху обектите в "
"Gimmie. Имате следните възможности: „single“ задейства обектите с еднократно "
"натискане, „double“ с двойно, а „nautilus“ повтаря настройката на Nautilus "
"за това."

#: ../data/gimmie.schemas.in.h:4
msgid "Display \"Computer\" topic"
msgstr "Показване на темата „Компютър“"

#: ../data/gimmie.schemas.in.h:5
msgid "Display Library topic"
msgstr "Показване на темата „Библиотека“"

#: ../data/gimmie.schemas.in.h:6
msgid "Display People topic"
msgstr "Показване на темата „Хора“"

#: ../data/gimmie.schemas.in.h:7
msgid "Display Programs topic"
msgstr "Показване на темата „Програми“"

#: ../data/gimmie.schemas.in.h:8
msgid "Display topic colors"
msgstr "Оцветяване на темите"

#: ../data/gimmie.schemas.in.h:9
msgid ""
"Each topic in Gimmie has an associated color. This key determines if the "
"color is displayed in panel buttons."
msgstr ""
"На всяка тема в Gimmie е присвоен цвят. Този ключ определя дали цветът се "
"показва в бутоните на панела."

#: ../data/gimmie.schemas.in.h:10
msgid "Include the \"Computer\" topic in this applet's list of topics."
msgstr "Включване на темата „Компютър“ в списъка на този аплет."

#: ../data/gimmie.schemas.in.h:11
msgid "Include the Library topic in this applet's list of topics."
msgstr "Включване на темата „Библиотека“ в списъка на този аплет."

#: ../data/gimmie.schemas.in.h:12
msgid "Include the People topic in this applet's list of topics."
msgstr "Включване на темата „Хора“ в списъка на този аплет."

#: ../data/gimmie.schemas.in.h:13
msgid "Include the Programs topic in this applet's list of topics."
msgstr "Включване на темата „Програми“ в списъка на този аплет."

#: ../data/gimmie.schemas.in.h:14
msgid ""
"Set how this applet's buttons are displayed. The following options are "
"supported: \"icons\" to show only Topic icons, \"text\" to show only Topic "
"names, and \"both\" to include both."
msgstr ""
"Как да се показват бутоните на аплета. Поддържат се следните възможности: "
"„icons“ - показват се само иконите на темите, „text“ - показват се само "
"имената на темите и „both“ - показват се и иконите, и имената."

#: ../data/GNOME_GimmieApplet.xml.h:1
msgid "_About"
msgstr "_Относно"

#: ../data/GNOME_GimmieApplet.xml.h:2
msgid "_Edit Menus"
msgstr "_Редактиране на менюта"

#: ../data/GNOME_GimmieApplet.xml.h:3
msgid "_Preferences"
msgstr "_Настройки"

#: ../gimmie/gimmie_applet.py:382
msgid "Favorite"
msgstr "Отметки"

#: ../gimmie/gimmie_applet.py:386 ../gimmie/gimmie_base.py:128
msgid "Add to Favorites"
msgstr "Добавяне към отметките"

#: ../gimmie/gimmie_applications.py:245
msgid "Other"
msgstr "Други"

#: ../gimmie/gimmie_applications.py:317
msgid "Install & Update"
msgstr "Инсталиране и обновяване"

#: ../gimmie/gimmie_applications.py:318
msgid "Manage software on this computer"
msgstr "Управление на програмите на този компютър"

#: ../gimmie/gimmie_applications.py:351 ../gimmie/gimmie_computer.py:578
#: ../gimmie/gimmie_computer.py:843
msgid "Settings"
msgstr "Настройки"

#: ../gimmie/gimmie_applications.py:352
msgid "Manage settings and preferences"
msgstr "Управление на настройките"

#: ../gimmie/gimmie_applications.py:368
msgid "Show Desktop"
msgstr "Показване на работния плот"

#: ../gimmie/gimmie_applications.py:387 ../gimmie/gimmie_prefs.py:72
msgid "Programs"
msgstr "Програми"

#: ../gimmie/gimmie_applications.py:409 ../gimmie/gimmie_recent.py:163
msgid "Recently Used"
msgstr "Скоро ползвани"

#: ../gimmie/gimmie_base.py:153
msgid "%l:%M %p"
msgstr "%H:%M"

#: ../gimmie/gimmie_base.py:156
#, python-format
msgid "Today, %s"
msgstr "Днес, %s"

#: ../gimmie/gimmie_base.py:160
#, python-format
msgid "Yesterday, %s"
msgstr "Вчера, %s"

#: ../gimmie/gimmie_base.py:162
#, python-format
msgid "%d day ago, %s"
msgid_plural "%d days ago, %s"
msgstr[0] "Преди %d ден, %s"
msgstr[1] "Преди %d дена, %s"

#: ../gimmie/gimmie_base.py:166
#, python-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "Преди %d ден"
msgstr[1] "Преди %d дена"

#: ../gimmie/gimmie_base.py:170
msgid "%B %e"
msgstr "%е %B"

#: ../gimmie/gimmie_base.py:172
msgid "%B %e, %G"
msgstr "%e %B, %y"

#: ../gimmie/gimmie_computer.py:44
msgid "All Favorites"
msgstr "Всички отметки"

#: ../gimmie/gimmie_computer.py:77 ../gimmie/gimmie_library.py:400
msgid "Places"
msgstr "Места"

#. FIXME: Check if drive is set to auto-mount, and if not show "Not Mounted"
#: ../gimmie/gimmie_computer.py:134
msgid "No disk inserted"
msgstr "Не е поставен диск"

#: ../gimmie/gimmie_computer.py:153
#, python-format
msgid "Free space: %s"
msgstr "Свободно място: %s"

#. FIXME: Add mount/unmount toggle?  Need to track nautilus.
#: ../gimmie/gimmie_computer.py:183
msgid "_Eject"
msgstr "_Изваждане"

#: ../gimmie/gimmie_computer.py:198
msgid "Devices & Media"
msgstr "Устройства и носители"

#: ../gimmie/gimmie_computer.py:203
msgid "Add Bluetooth..."
msgstr "Добавяне на Блутут..."

#: ../gimmie/gimmie_computer.py:204
msgid "Access a wireless device"
msgstr "Достъп до безжично устройство"

#: ../gimmie/gimmie_computer.py:209
msgid "Create CD/DVD..."
msgstr "Създаване на CD/DVD..."

#: ../gimmie/gimmie_computer.py:210
msgid "Burn files onto a CD or DVD disk"
msgstr "Копиране на файлове върху CD/DVD"

#: ../gimmie/gimmie_computer.py:339 ../gimmie/gimmie_computer.py:456
#: ../gimmie/gimmie_library.py:420
msgid "Printers"
msgstr "Принтери"

#: ../gimmie/gimmie_computer.py:344 ../gimmie/gimmie_computer.py:461
msgid "Add Printer..."
msgstr "Добавяне на принтер..."

#: ../gimmie/gimmie_computer.py:345 ../gimmie/gimmie_computer.py:462
msgid "Setup attached or networked printer"
msgstr "Настройване на локален или мрежови принтер"

#: ../gimmie/gimmie_computer.py:536
msgid "Nearby Computers"
msgstr "Компютри в околността"

#: ../gimmie/gimmie_computer.py:541
msgid "Connect to..."
msgstr "Свързване към..."

#: ../gimmie/gimmie_computer.py:542
msgid "Access a computer on the network"
msgstr "Достъп до компютър през мрежата"

#: ../gimmie/gimmie_computer.py:547
msgid "Windows Network"
msgstr "Мрежа на Уиндоус"

#: ../gimmie/gimmie_computer.py:548
msgid "Browse nearby Windows computers"
msgstr "Разглеждане на компютрите с Уиндоус в околността"

#: ../gimmie/gimmie_computer.py:637
msgid "Administration"
msgstr "Администриране"

#: ../gimmie/gimmie_computer.py:676
msgid "About Me"
msgstr "За мен"

#: ../gimmie/gimmie_computer.py:682
msgid "Switch User"
msgstr "Смяна на потребител"

#: ../gimmie/gimmie_computer.py:688
msgid "Log Out..."
msgstr "Изход..."

#: ../gimmie/gimmie_computer.py:716
msgid "Switch to another user"
msgstr "Смяна към друг потребител"

#: ../gimmie/gimmie_computer.py:733
msgid "Shutdown..."
msgstr "Спиране на компютъра..."

#: ../gimmie/gimmie_computer.py:734
msgid "Shutdown or suspend this computer"
msgstr "Спиране или приспиване на компютъра"

#: ../gimmie/gimmie_computer.py:744
msgid "Help"
msgstr "Помощ"

#: ../gimmie/gimmie_computer.py:749
msgid "About Gnome"
msgstr "Относно Gnome"

#: ../gimmie/gimmie_computer.py:761
msgid "Volume: 75%"
msgstr "Сила на звука: 75%"

#: ../gimmie/gimmie_computer.py:766 ../gimmie/gimmie_computer.py:782
msgid "Not Implemented"
msgstr "Не е реализирано"

#: ../gimmie/gimmie_computer.py:777
msgid "Change the network location"
msgstr "Смяна на мястото в мрежата"

#: ../gimmie/gimmie_computer.py:844
msgid "Preferences & Administration"
msgstr "Настройки и администриране"

#: ../gimmie/gimmie_computer.py:862 ../gimmie/gimmie_prefs.py:69
msgid "Computer"
msgstr "Компютър"

#: ../gimmie/gimmie_computer.py:954
msgid "Computer, %l:%M %p"
msgstr "Компютър, %H:%M"

#: ../gimmie/gimmie_facebook.py:130
msgid "Unknown"
msgstr "Непознат"

#: ../gimmie/gimmie_facebook.py:192 ../gimmie/gimmie_facebook.py:247
msgid "Login to Facebook"
msgstr "Влизане във Facebook"

#: ../gimmie/gimmie_facebook.py:193
msgid "Access your Facebook friends"
msgstr "Достъп до приятелите ви във Facebook"

#: ../gimmie/gimmie_flickr.py:141
msgid "Tags: "
msgstr "Етикети:"

#: ../gimmie/gimmie_flickr.py:165 ../gimmie/gimmie_flickr.py:220
msgid "Login to Flickr"
msgstr "Влизане във Flickr"

#: ../gimmie/gimmie_flickr.py:166
msgid "Access your Flickr photos"
msgstr "Достъп до снимките ви във Flickr"

#: ../gimmie/gimmie_flickr.py:232
msgid "Flickr Photos"
msgstr "Снимки във Flickr"

#: ../gimmie/gimmie_gaim.py:101 ../gimmie/gimmie_pidgin.py:120
#, python-format
msgid "Idle since %s"
msgstr "Бездействие от %s"

#: ../gimmie/gimmie_gaim.py:104 ../gimmie/gimmie_pidgin.py:123
msgid "Idle"
msgstr "Липса на активност"

#: ../gimmie/gimmie_gaim.py:106 ../gimmie/gimmie_people.py:97
#: ../gimmie/gimmie_people.py:214 ../gimmie/gimmie_pidgin.py:125
msgid "Away"
msgstr "Отсъстващ"

#: ../gimmie/gimmie_gaim.py:109 ../gimmie/gimmie_people.py:91
#: ../gimmie/gimmie_people.py:212 ../gimmie/gimmie_pidgin.py:128
#: ../gimmie/gimmie_pidgin.py:567
msgid "Offline"
msgstr "Изключен"

#: ../gimmie/gimmie_gmail.py:36 ../gimmie/gimmie_gmail.py:155
msgid "Login to Gmail"
msgstr "Влизане в Gmail"

#: ../gimmie/gimmie_gmail.py:47 ../gimmie/gimmie_googleoffice.py:52
msgid "Account Settings"
msgstr "Настройки на абонамента"

#: ../gimmie/gimmie_gmail.py:61
msgid "Sign in to Gmail with your Google Account"
msgstr "Влизане в GMail с абонамента ви за Google"

#: ../gimmie/gimmie_gmail.py:71 ../gimmie/gimmie_googleoffice.py:76
msgid "Username:"
msgstr "Потребителско име:"

#: ../gimmie/gimmie_gmail.py:76 ../gimmie/gimmie_googleoffice.py:81
msgid "Password:"
msgstr "Парола:"

#: ../gimmie/gimmie_gmail.py:276 ../gimmie/gimmie_googleoffice.py:292
#, python-format
msgid ""
"Login failed for %s:\n"
"%s"
msgstr ""
"Неуспешно влизане с име %s:\n"
"%s"

#: ../gimmie/gimmie_gmail.py:280 ../gimmie/gimmie_googleoffice.py:296
msgid "_Ignore"
msgstr "_Пренебрегване"

#: ../gimmie/gimmie_gmail.py:282 ../gimmie/gimmie_googleoffice.py:298
#: ../gimmie/gimmie_trash.py:249
msgid "_Retry"
msgstr "_Нов опит"

#: ../gimmie/gimmie_googleoffice.py:41 ../gimmie/gimmie_googleoffice.py:160
msgid "Login to Google Office"
msgstr "Влизане в Google Office"

#: ../gimmie/gimmie_googleoffice.py:66
msgid "Sign in to Google Office with your Google Account"
msgstr "Влизане в Google Office с абонамента ви за Google"

#: ../gimmie/gimmie_googleoffice.py:188
msgid "Google Docs"
msgstr "Документи в Google"

#: ../gimmie/gimmie_gui.py:691
msgid "Calendar"
msgstr "Календар"

#: ../gimmie/gimmie_gui.py:792
#, python-format
msgid "Desk %d"
msgstr "Работен плот %d"

#: ../gimmie/gimmie_gui.py:875
msgid "Use Compact Layout"
msgstr "Компактна подредба"

#: ../gimmie/gimmie_library.py:137
msgid "Attachments"
msgstr "Притурки"

#: ../gimmie/gimmie_library.py:149
msgid "Downloads"
msgstr "Свалени файлове"

#: ../gimmie/gimmie_library.py:207
msgid "Documents"
msgstr "Документи"

#: ../gimmie/gimmie_library.py:226
msgid "Music & Movies"
msgstr "Музика и филми"

#: ../gimmie/gimmie_library.py:257 ../gimmie/gimmie_library.py:294
msgid "New Document"
msgstr "Нов документ"

#: ../gimmie/gimmie_library.py:295
msgid "Create a new document"
msgstr "Създаване на нов документ"

#: ../gimmie/gimmie_library.py:331
msgid "No templates available"
msgstr "Не са налични шаблони"

#: ../gimmie/gimmie_library.py:340
msgid "Empty File"
msgstr "Празен файл"

#. Add a link to the templates directory
#: ../gimmie/gimmie_library.py:347
msgid "Templates"
msgstr "Шаблони"

#: ../gimmie/gimmie_library.py:385 ../gimmie/gimmie_library.py:387
#: ../gimmie/gimmie_library.py:408 ../gimmie/gimmie_trash.py:152
msgid "Trash"
msgstr "Кошче"

#: ../gimmie/gimmie_library.py:401
msgid "Open commonly used locations"
msgstr "Отваряне на често ползваните места"

#: ../gimmie/gimmie_library.py:409
msgid "Open the deleted files trashcan"
msgstr "Отваряне на кошчето, където са изтритите файлове"

#: ../gimmie/gimmie_library.py:421
msgid "View printers available for document printing"
msgstr "Разглеждане на принтерите, които ви предлагат да разпечатвате"

#: ../gimmie/gimmie_library.py:443 ../gimmie/gimmie_prefs.py:73
msgid "Library"
msgstr "Библиотека"

#: ../gimmie/gimmie_logout.py:53
msgid "Shut down this computer now?"
msgstr "Искате ли да спрете компютъра?"

#: ../gimmie/gimmie_logout.py:57
msgid "_Sleep"
msgstr "_Приспиване"

#: ../gimmie/gimmie_logout.py:62
msgid "_Hibernate"
msgstr "_Дълбоко приспиване"

#: ../gimmie/gimmie_logout.py:68
msgid "_Reboot"
msgstr "_Рестартиране"

#: ../gimmie/gimmie_logout.py:74
msgid "Shut _Down"
msgstr "_Спиране на компютъра"

#: ../gimmie/gimmie_logout.py:81
msgid "Log out of this computer now?"
msgstr "Искате ли да излезете от системата?"

#: ../gimmie/gimmie_logout.py:85
msgid "_Log Out"
msgstr "_Изход от системата"

#: ../gimmie/gimmie_logout.py:175
#, python-format
msgid "This computer will be automatically shut down in %d second."
msgid_plural "This computer will be automatically shut down in %d seconds."
msgstr[0] "Този компютър ще спре автоматично след %d секунда."
msgstr[1] "Този компютър ще спре автоматично след %d секунди."

#: ../gimmie/gimmie_logout.py:179
#, python-format
msgid "You will be automatically logged out in %d second."
msgid_plural "You will be automatically logged out in %d seconds."
msgstr[0] "Автоматично ще излезете от системата след %d секунда."
msgstr[1] "Автоматично ще излезете от системата след %d секунди."

#: ../gimmie/gimmie_people.py:85 ../gimmie/gimmie_people.py:210
msgid "Available"
msgstr "На линия"

#: ../gimmie/gimmie_people.py:103 ../gimmie/gimmie_people.py:216
msgid "Invisible"
msgstr "Невидим"

#: ../gimmie/gimmie_people.py:162
msgid "New Person"
msgstr "Нов човек"

#: ../gimmie/gimmie_people.py:163
msgid "Add a new contact person"
msgstr "Добавяне на нов човек в контактите"

#: ../gimmie/gimmie_people.py:191
msgid "Status"
msgstr "Състояние"

#: ../gimmie/gimmie_people.py:193
msgid "Set your online status"
msgstr "Задайте състоянието си по мрежата"

#: ../gimmie/gimmie_people.py:240 ../gimmie/gimmie_prefs.py:74
msgid "People"
msgstr "Хора"

#: ../gimmie/gimmie_people.py:254
msgid "Recent People"
msgstr "Хора, с които скоро сте говорили"

#: ../gimmie/gimmie_people.py:255
msgid "Online Now"
msgstr "В момента е на линия"

#: ../gimmie/gimmie_people.py:256
msgid "Everybody"
msgstr "Всички"

#: ../gimmie/gimmie_people.py:261
msgid "Gmail"
msgstr "GMail"

#: ../gimmie/gimmie_people.py:264
msgid "GMail"
msgstr "GMail"

#: ../gimmie/gimmie_people.py:268 ../gimmie/gimmie_people.py:271
msgid "Facebook"
msgstr "Facebook"

#: ../gimmie/gimmie_prefs.py:14
msgid "Gimmie Preferences"
msgstr "Настройки на Gimmie"

#: ../gimmie/gimmie_prefs.py:23
msgid "Text only"
msgstr "Само текст"

#: ../gimmie/gimmie_prefs.py:24
msgid "Icons only"
msgstr "Само икони"

#: ../gimmie/gimmie_prefs.py:25
msgid "Text and icons"
msgstr "Икони с текст"

#: ../gimmie/gimmie_prefs.py:38
msgid "Topic _button labels:"
msgstr "_Етикети на бутоните за темите:"

#: ../gimmie/gimmie_prefs.py:60
msgid "Appearance"
msgstr "Външен вид"

#: ../gimmie/gimmie_prefs.py:81
msgid "Topics"
msgstr "Теми"

#: ../gimmie/gimmie_tomboy.py:89
msgid "Notes"
msgstr "Бележки"

#: ../gimmie/gimmie_tomboy.py:93
msgid "Create New Note"
msgstr "Създаване на нова бележка"

#: ../gimmie/gimmie_tomboy.py:94
msgid "Make a new Tomboy note"
msgstr "Нова бележка в Tomboy"

#: ../gimmie/gimmie_topicwin.py:44 ../gimmie/gimmie_topicwin.py:1241
msgid "Search"
msgstr "Търсене"

#: ../gimmie/gimmie_topicwin.py:165 ../gimmie/gimmie_topicwin.py:488
msgid "Today"
msgstr "Днес"

#: ../gimmie/gimmie_topicwin.py:166 ../gimmie/gimmie_topicwin.py:489
msgid "This week"
msgstr "Тази седмица"

#: ../gimmie/gimmie_topicwin.py:167 ../gimmie/gimmie_topicwin.py:490
msgid "Last 2 weeks"
msgstr "Последните две седмици"

#: ../gimmie/gimmie_topicwin.py:168 ../gimmie/gimmie_topicwin.py:491
msgid "This month"
msgstr "Този месец"

#: ../gimmie/gimmie_topicwin.py:169 ../gimmie/gimmie_topicwin.py:492
msgid "Last 2 months"
msgstr "Последните два месеца"

#: ../gimmie/gimmie_topicwin.py:170 ../gimmie/gimmie_topicwin.py:493
msgid "Last 3 months"
msgstr "Последните 3 месеца"

#: ../gimmie/gimmie_topicwin.py:171 ../gimmie/gimmie_topicwin.py:494
msgid "Last 6 months"
msgstr "Последните 6 месеца     "

#: ../gimmie/gimmie_topicwin.py:193
msgid "Show timeline"
msgstr "Показване на оста на времето"

#: ../gimmie/gimmie_topicwin.py:1296
msgid "Set the zoom level"
msgstr "Мащаб"

#: ../gimmie/gimmie_trash.py:173 ../gimmie/gimmie_trash.py:213
msgid "_Empty Trash"
msgstr "_Изчистване на кошчето"

#: ../gimmie/gimmie_trash.py:180
#, python-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] "%d обект"
msgstr[1] "%d обекта"

#: ../gimmie/gimmie_trash.py:205
msgid "Empty all of the items from the trash?"
msgstr "Изтриване на всички обекти от кошчето?"

#: ../gimmie/gimmie_trash.py:206
msgid ""
"If you choose to empty the trash, all items in it will be permanently lost.  "
"Please note that you can also delete them separately."
msgstr ""
"Ако изчистите кошчето, обектите в него ще бъдат окончателно изтрити. Ако "
"искате, можете да ги триете и поотделно."

#: ../gimmie/gimmie_trash.py:229
msgid "Error while moving."
msgstr "Грешка при преместване."

#: ../gimmie/gimmie_trash.py:230
#, python-format
msgid ""
"Cannot move \"%s\" to the trash because you do not have permissions to "
"change it or its parent folder."
msgstr "Обектът „%s“ не може да бъде преместен в кошчето, защото нямате права да го променяте или да променяте папката, в която се намира."

#: ../gimmie/gimmie_trash.py:232
msgid "Error while deleting."
msgstr "Грешка при изтриване."

#: ../gimmie/gimmie_trash.py:233
#, python-format
msgid ""
"\"%s\" cannot be deleted because you do not have permissions to modify its "
"parent folder."
msgstr ""
"Обектът „%s“ не може да бъде изтрит, защото нямате права да променяте "
"папката, в която се намира."

#: ../gimmie/gimmie_trash.py:235
msgid "Error while performing file operation."
msgstr "Грешка при извършване на файловата операция."

#: ../gimmie/gimmie_trash.py:236
#, python-format
msgid "Cannot perform file operation %d on \"%s\"."
msgstr "Файловата операция %d не може да бъде изпълнена върху „%s“."

#: ../gimmie/gimmie_trash.py:243
msgid "_Skip"
msgstr "_Пропускане"

#: ../gimmie/gimmie_trash.py:303
msgid "Cannot move items to trash, do you want to delete them immediately?"
msgstr ""
"Обектите не могат да бъдат преместени в кошчето. Искате ли да ги изтриете "
"незабавно?"

#: ../gimmie/gimmie_trash.py:304
#, python-format
msgid "None of the %d selected items can be moved to the Trash"
msgstr "Никой от избраните %d обекта не може да бъде преместен в кошчето."

#: ../gimmie/gimmie_trash.py:306
msgid ""
"Cannot move some items to trash, do you want to delete these immediately?"
msgstr ""
"Някои от обектите не могат да бъдат преместени в кошчето. Искате ли да ги "
"изтриете незабавно?"

#: ../gimmie/gimmie_trash.py:307
#, python-format
msgid "%d of the selected items cannot be moved to the Trash"
msgstr "%d от избраните обекти не могат да бъдат преместени в кошчето."

#: ../gimmie/gimmie_trash.py:317
msgid "_Delete"
msgstr "_Изтриване"

#: ../gimmie/gimmie_util.py:832
msgid "Home"
msgstr "Домашна папка"

#: ../gimmie/gimmie_util.py:836
msgid "Desktop"
msgstr "Работен плот"

#: ../gimmie/gimmie_util.py:893
msgid "Not Implemented..."
msgstr "Не е реализирано..."

#: ../gimmie/gimmie_util.py:898
#, python-format
msgid ""
"Help us to write this feature by editing the file \"%s\" at line number %d."
msgstr "Помогнете ни да реализираме тази възможност като редактирате файла „%s“, ред № %d."

#: ../gimmie/gimmie_util.py:903
msgid "_Fix It!"
msgstr "_Поправи го!"

#: ../gimmie/traymanager/eggtraymanager.c:146
msgid "Orientation"
msgstr "Ориентация"

#: ../gimmie/traymanager/eggtraymanager.c:147
msgid "The orientation of the tray."
msgstr "Ориентацията на тавата."
